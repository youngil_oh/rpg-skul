using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Item")]
public class Item : ScriptableObject
{
    public string objectName;

    public Sprite sprite;

    public int quantity;  // 아이템의 수량

    public bool stackable; // 인벤토리에 넣을때 동일한 아이템의 중첩 가능한지 (false / true)

    public enum ItemType
    {
        COIN,
        HEALTH
    }

    public ItemType itemType;    
}
