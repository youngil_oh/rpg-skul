using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPGGameManager : MonoBehaviour
{
    public static RPGGameManager sharedInstance = null;

    private void Awake()
    {
        if(sharedInstance != null && sharedInstance != this)
        {
            Destroy(gameObject);
        }
        else // 현재 인스턴스가 유일한 인스턴스 일때 
        {
            sharedInstance = this;
        }
    }

    public RPGCameraManager cameraManager;
    public SpawnPoint playerSpawnPoint;

    private void Start()
    {
        SetupScens();
    }

    public void SetupScens()
    {
        SpawnPlayer();
    }

    public void SpawnPlayer()
    {
        if(playerSpawnPoint != null)
        {
            GameObject player = playerSpawnPoint.SpawnObject();

            cameraManager.virtualCamera.Follow = player.transform;
        }
    }
}
