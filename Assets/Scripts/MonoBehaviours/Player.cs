using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    public HitPoints hitPoints; // 현재 체력
    public Inventory inventoryPrefab;
    public HealthBar healthBarPrefab; // HealthBar 프리펩의 참조를 저장한다.

    Inventory inventory;
    HealthBar healthBar; // 인스턴스화한 HealthBar의 참조를 저장한다.   

    private void OnEnable()
    {
        ResetCharacter();
    }

    private void Start()
    {
        inventory = Instantiate(inventoryPrefab);

        hitPoints.value = startingHitPoints; // 플레이어의 체력 startingHitPoints 값으로 시작한다. 

        healthBar = Instantiate(healthBarPrefab); // HealthBar 프리펩의 복사복을 인스터스화 한다.

        healthBar.character = this;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("CanBePickedUp"))
        {
            Item hitObject = collision.gameObject.GetComponent<Consumable>().item;

            if (hitObject != null)
            {
                bool shouldDisappear = false; // 충돌한 오브젝트를 감춰야 할지 나타내는 변수

                switch (hitObject.itemType)
                {
                    case Item.ItemType.COIN:
                        shouldDisappear = inventory.AddItem(hitObject);
                        break;

                    case Item.ItemType.HEALTH:
                        shouldDisappear = AdjustHitPoints(hitObject.quantity);
                        break;

                    default:
                        break;
                }

                if (shouldDisappear)
                {
                    collision.gameObject.SetActive(false);
                }                
            }
        }        
    }

    public bool AdjustHitPoints(int amount)
    {
        if (hitPoints.value < maxHitPoints) // 현재 체력 값이 체력 값의 최대 허용치보다 적은지 확인한다.
        {
            hitPoints.value = hitPoints.value + amount;
            print("Adjustrd hitpoints by" + amount + ". New value : " + hitPoints.value);

            return true;
        }      

        return false;
    }

    public override void ResetCharacter()
    {
        inventory = Instantiate(inventoryPrefab);
        healthBar = Instantiate(healthBarPrefab);
        healthBar.character = this;

        hitPoints.value = startingHitPoints;
    }

    public override IEnumerator DamageCharacter(int damage, float interval)
    {
        while (true)
        {
            hitPoints.value = hitPoints.value - damage;

            if(hitPoints.value <= float.Epsilon)
            {
                KillCharanter();
                break;
            }

            if(interval > float.Epsilon)
            {
                yield return new WaitForSeconds(interval);
            }
            else
            {
                break;
            }
        }
        yield return null;
    }

    public override void KillCharanter()
    {
        base.KillCharanter(); 

        Destroy(healthBar.gameObject);
        Destroy(inventory.gameObject);
    }
}
