using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public float movementSpeed = 3.0f;
    Vector2 movement = new Vector2();

    Animator animator;

    readonly string animationState = "AnimationState";  // readonly 사용하면 가비지가 발생하지 않는다.
    Rigidbody2D rd2D;

    enum CharStates
    {
        walkEast = 1,
        walkSouth = 2,
        walkWest = 3,
        walkNorth = 4,
        idleSouth = 5,
    }

    private void Start()
    {
        animator = this.GetComponent<Animator>();
        rd2D = this.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        UpdateState();
    }

    private void FixedUpdate()
    {
        MoveCharacter();
    }
    private void MoveCharacter()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        movement.Normalize();

        rd2D.velocity = movement * movementSpeed;  // velocity 현재 프레임에서 의 이동 속도 
    }

    private void UpdateState()
    {
        if (movement.x > 0)
        { // 오른쪽으로 이동
            animator.SetInteger(animationState, (int)CharStates.walkEast);
        }
        else if (movement.x < 0)
        { // 왼쪽으로 이동
            animator.SetInteger(animationState, (int)CharStates.walkWest);
        }
        else if (movement.y < 0)
        { // 아래쪽으로 이동
            animator.SetInteger(animationState, (int)CharStates.walkSouth);
        }
        else if (movement.y > 0)
        { // 위쪽으로 이동
            animator.SetInteger(animationState, (int)CharStates.walkNorth);
        }
        else
        { // 키입력이 없을때        
            animator.SetInteger(animationState, (int)CharStates.idleSouth);                      
        }
    }   
}
