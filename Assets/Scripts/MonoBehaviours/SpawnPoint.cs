using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public GameObject prefabToSpawn;
    public float repeatInterval; // 일정한 간격을 두고 프리팹을 호출할 시간

    private void Start()
    {
        if(repeatInterval > 0)
        {
            /* 
             *  Invoke : 일정한 시간 만큼 지연된 수 실행이 되는 함수 (한번만 실행)
             *  InvokeRepeating : 일정한 시간 만큼 지연된 수 실행이 되는 함수 (반복 실행)
             *  CancleRepeating : 실행중인 Invoke 관련 함수를 중지한다.
            */
            InvokeRepeating("SpawnObject", 0.0f/*지연시간*/, repeatInterval/*반복주기*/);
        }
    }

    public GameObject SpawnObject()
    {
        if(prefabToSpawn != null)
        {
            return Instantiate(prefabToSpawn, transform.position, Quaternion.identity);
        }
        return null;
    }
}
