using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public GameObject slotPrefab; // Slot 프리팹의 참조를 저장한다.

    public const int numSlots = 5; // 인베토리 바의 슬롯은 다섯개로 설정한다.

    Image[] itemImages = new Image[numSlots];  // Image 컴포넌트를 저장할 배열. Image 컨포넌트를 저장한다.

    Item[] items = new Item[numSlots]; // 플레이어가 획득한 아이템의 참조를 저장할 변수

    GameObject[] slots = new GameObject[numSlots]; // slots 배열의 각 인덱스는 Slot 프리팹을 가리킨다.

    private void Start()
    {
        CreatSlots();
    }

    public void CreatSlots()
    {
        if(slotPrefab != null) // Slot 프리팹이 유효한지 체크한다.
        {
            for(int i = 0; i < numSlots; i++) // 슬롯의 개수만큼 반복한다. (류프를 실행한다)
            {
                GameObject newSlot = Instantiate(slotPrefab); // Slot 프리팹의 복사본을 인스터스화 해서 newSlot에 할당한다.

                newSlot.name = "ItemSlot_" + i; // 인덱스 번호를 붙여서 게임 오브젝트 이름을 변경한다.

                newSlot.transform.SetParent(gameObject.transform.GetChild(0).transform); // 인덱스 0 에 해당하는 자식 오브젝트는 InventoryBackGround

                slots[i] = newSlot; // 새로 생성된 Slot 오브젝트를 slots 배열의 현재 인덱스에 대입한다. 

                itemImages[i] = newSlot.transform.GetChild(1).GetComponent<Image>(); // 인덱스 1에 해당하는 자식 오브젝트는 ItemImage 이다.
            }
        }
    }

    public bool AddItem(Item itemToAdd)
    {
        for(int i = 0; i < items.Length; i++)
        {
            if(items[i] != null && items[i].itemType == itemToAdd.itemType && itemToAdd.stackable == true) /*인베토리에 추가하려는 아이템의
                                                                                                            itemType과 items에 있는 아이템이
                                                                                                            itemType이 같은지 확인한다.*/
            {
                // 모든 조건이 맞으면 원래 있던 아이템에 새로운 아이템을 추가한다.
                items[i].quantity = items[i].quantity + 1;
                Slot slotScript = slots[i].gameObject.GetComponent<Slot>();
                Text quantityText = slotScript.qtyText;
                quantityText.enabled = true;
                quantityText.text = items[i].quantity.ToString(); // int 형식을 String 형식으로 변환한다.
                return true;
            }            
        
            else if(items[i] == null)
            {
                items[i] = Instantiate(itemToAdd);
                items[i].quantity = 1;
                itemImages[i].sprite = itemToAdd.sprite;
                itemImages[i].enabled = true;
                return true;
            }
        }
        return true;
    }
}
