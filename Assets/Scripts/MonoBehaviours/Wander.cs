using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))] // RequireComponent : 설정 오류를 막기위해 자동으로 추가한다.
[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(Animator))]

public class Wander : MonoBehaviour
{
    CircleCollider2D circleCollider;

    public float pursuitSpeed; // 추적할떄 속력
    public float wanderSpeed; // 배회 속력
    float currentSpeed; // 현재 속력

    public float directionChangeInterval; // 배회할 방향의 전환 빈도를 결정
    public bool followPlayer; // 추적하는 기능을 켜거나 끌 수 있는 플래그
    Coroutine moveCoroutine; // 이동 코루틴의 참조 저장

    Rigidbody2D rd2d; // 참조형 변수
    Animator animator; // 참조형 변수

    Transform targetTransform = null; // 적이 플레이어를 추적할 때 사용 
    Vector3 endPosition; // 목적지
    float currentAngle = 0f; // 배회할 방향을 바꿀 떄 기존 각도에 새로운 각도를 더한다

    private void Start()
    {
        animator = GetComponent<Animator>();
        currentSpeed = wanderSpeed;
        rd2d = GetComponent<Rigidbody2D>();
        StartCoroutine(WanderRoutine());
        circleCollider = GetComponent<CircleCollider2D>();
    }

    private void OnDrawGizmos()
    {
        if(circleCollider != null)
        {
            Gizmos.DrawWireSphere(transform.position, circleCollider.radius);
        }
    }

    private void Update()
    {
        Debug.DrawLine(rd2d.position, endPosition, Color.red);
    }

    public IEnumerator WanderRoutine()
    {
        while (true)
        {
            ChooseNewEndpoint();

            if(moveCoroutine != null)
            {
                StopCoroutine(moveCoroutine);
            }

            moveCoroutine = StartCoroutine(Move(rd2d, currentSpeed));
            yield return new WaitForSeconds(directionChangeInterval);
        }
        yield return null;
    }

    void ChooseNewEndpoint()
    {
        currentAngle += Random.Range(0, 360);
        currentAngle = Mathf.Repeat(currentAngle, 360);
        endPosition = this.transform.position + Vector3FormAngle(currentAngle);
    }

    Vector3 Vector3FormAngle(float inputAngleDegrees)
    {
        float inputAngleRadians = inputAngleDegrees * Mathf.Deg2Rad;
        return new Vector3(Mathf.Cos(inputAngleRadians), Mathf.Sin(inputAngleRadians), 0);
    }

    public IEnumerator Move(Rigidbody2D rigidBodyToMove, float speed)
    {
        float remainingDistance = (transform.position - endPosition).sqrMagnitude;
        while(remainingDistance > float.Epsilon)
        {
            if(targetTransform != null)
            {
                endPosition = targetTransform.position;
            }

            if(rigidBodyToMove != null)
            {
                animator.SetBool("isWalking", true);
                Vector3 newPosition = Vector3.MoveTowards(rigidBodyToMove.position, endPosition, speed * Time.deltaTime);
                rd2d.MovePosition(newPosition);
                remainingDistance = (transform.position - endPosition).sqrMagnitude;
            }

            yield return new WaitForFixedUpdate();
        }

        animator.SetBool("isWalking", false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player") && followPlayer)
        {
            currentSpeed = pursuitSpeed;

            targetTransform = collision.gameObject.transform;
            if(moveCoroutine != null)
            {
                StopCoroutine(moveCoroutine);
            }

            moveCoroutine = StartCoroutine(Move(rd2d, currentSpeed));
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && followPlayer)
        {
            animator.SetBool("isWalking", false);
            currentSpeed = wanderSpeed;
            
            if (moveCoroutine != null)
            {
                StopCoroutine(moveCoroutine);
            }
        }         

        targetTransform = null;
    }
}
