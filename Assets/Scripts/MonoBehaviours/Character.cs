using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{        
    public float maxHitPoints; // 최대 체력
    public float startingHitPoints; // 최초 체력


    public virtual void KillCharanter()
    {
        Destroy(gameObject);
    }

    public abstract void ResetCharacter();

    public abstract IEnumerator DamageCharacter(int damage, float interval);


}
