using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arc : MonoBehaviour
{
    public IEnumerator TraveArc(Vector3 destination, float duration)
    {
        var startPosiiton = transform.position;
        var percentComplete = 0.0f;

        while(percentComplete < 1.0f)
        {
            percentComplete += Time.deltaTime / duration;
            var currenHeight = Mathf.Sin(Mathf.PI * percentComplete);

            transform.position = Vector3.Lerp(startPosiiton, destination, percentComplete) + Vector3.up * currenHeight;
            yield return null;
        }

        gameObject.SetActive(false);
    }
}
